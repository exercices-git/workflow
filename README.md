# Exercice Workflow

Groupez-vous par 2 et créez un nouveau repository.

L'un de vous doit créer un fichier README.md sur la branche `main` ou `master`

Créez 2 nouvelles issues :
- Créer un fichier voiture.yaml
- Créer un fichier velo.yaml

(la première sera l'issue #1 et la seconde #2)

Affecter une issue à l'un et la seconde à l'autre contributeur du projet.

Chacun de votre côté clonez le repository puis créez un branche en partant de `main` ou `master`.

Votre branche s'appellera `feat/voiture` ou `feat/velo` selon le ticket qui vous a été affecté.

Voici le contenu du fichier `voiture.yaml` :

```
voiture: 
  marque: Renault
  modele: Twingo
```

Voici le contenu du fichier `velo.yaml` :

```
velo: 
  marque: BMC
  modele: Alpenchallenge
```

Créer un commit avec ce fichier et faire un `push` sur la branche correspondante du remote.

Créer ensuite une Pull Request ou Merge Request sur la plateforme.

L'autre contributeur doit vous demander de faire un changement (exemple changer de marque et/ou de modèle)

Refaire un commit avec cette modification puis faire un push.

Ensuite faire le merge (squash ou pas à vous de voir) sur la plateforme en ajoutant dans le message de commit :
Closes #1

(où 1 est le numéro de ticket correspondant)

Le ticket doit se fermer tout seul au moment du merge.

Pour finir, sur votre poste local récupérer les commits de `main` ou `master` et supprimer les autres branches (local et remote)
